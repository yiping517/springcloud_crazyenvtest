package org.crazyit.cloud;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import jdk.nashorn.internal.ir.RuntimeNode.Request;

//@Controller
@RestController
public class MyController {

	@GetMapping("/hello")
//	@ResponseBody
	public String hello() {
		return "Hello World!";
	}
	
	@RequestMapping(value="/person/{personID}",method=RequestMethod.GET,
			produces=MediaType.APPLICATION_JSON_VALUE)
	public Person findPerson(@PathVariable("personID") Integer personID) {
		Person p = new Person();
		p.setId(personID);
		p.setName("Crazyit");
		p.setAge(30);
		return p;
	}
}
